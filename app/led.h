/**
 ******************************************************************************
 * @file    led.h 
 * @author  jobs 
 * @version v0.0.0 
 * @date    2021-11-06 
 * @brief   
 * @note    
 ******************************************************************************
 */
#ifndef LED_H 
#define LED_H 

#if defined(__cplusplus)
extern "C" {
#endif /* __cplusplus*/

extern uint8_t led_disp_buf[5];

extern void led_init(void);
extern void led_blink(void);
extern void led_display(void);

#if defined(__cplusplus)
}
#endif /* __cplusplus*/
#endif

/******************************** END OF FILE *********************************/
