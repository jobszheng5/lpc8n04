/**
******************************************************************************
* @file    led.c 
* @author  jobs
* @version v0.00
* @date    2021-11-06
* @brief   
* @note    
*
******************************************************************************
*/

#include <stdint.h>
#include "led.h"
#include "chip.h"

uint8_t led_disp_buf[5] = {0x00, 0x00, 0x00, 0x00, 0x00};
const uint8_t led_seg_pin[5] = {6, 5, 4, 10, 11};

void led_init(void)
{
  Chip_IOCON_SetPinConfig(LPC_IOCON, IOCON_PIO0_4, (IOCON_FUNC_0 | IOCON_RMODE_PULLDOWN));
  Chip_GPIO_SetPinDIROutput(LPC_GPIO, 0, 4);
  Chip_GPIO_SetPinOutHigh(LPC_GPIO, 0, 4);
  Chip_IOCON_SetPinConfig(LPC_IOCON, IOCON_PIO0_5, (IOCON_FUNC_0 | IOCON_RMODE_PULLDOWN));
  Chip_GPIO_SetPinDIROutput(LPC_GPIO, 0, 5);
  Chip_GPIO_SetPinOutHigh(LPC_GPIO, 0, 5);
  Chip_IOCON_SetPinConfig(LPC_IOCON, IOCON_PIO0_6, (IOCON_FUNC_0 | IOCON_RMODE_PULLDOWN));
  Chip_GPIO_SetPinDIROutput(LPC_GPIO, 0, 6);
  Chip_GPIO_SetPinOutHigh(LPC_GPIO, 0, 6);
  Chip_IOCON_SetPinConfig(LPC_IOCON, IOCON_PIO0_0, (IOCON_FUNC_0 | IOCON_RMODE_PULLDOWN));
  Chip_GPIO_SetPinDIROutput(LPC_GPIO, 0, 0);
  Chip_GPIO_SetPinOutLow(LPC_GPIO, 0, 0);
  Chip_IOCON_SetPinConfig(LPC_IOCON, IOCON_PIO0_1, (IOCON_FUNC_0 | IOCON_RMODE_PULLDOWN));
  Chip_GPIO_SetPinDIROutput(LPC_GPIO, 0, 1);
  Chip_GPIO_SetPinOutLow(LPC_GPIO, 0, 1);
  Chip_IOCON_SetPinConfig(LPC_IOCON, IOCON_PIO0_2, (IOCON_FUNC_0 | IOCON_RMODE_PULLDOWN));
  Chip_GPIO_SetPinDIROutput(LPC_GPIO, 0, 2);
  Chip_GPIO_SetPinOutLow(LPC_GPIO, 0, 2);
  Chip_IOCON_SetPinConfig(LPC_IOCON, IOCON_PIO0_3, (IOCON_FUNC_0 | IOCON_RMODE_PULLDOWN));
  Chip_GPIO_SetPinDIROutput(LPC_GPIO, 0, 3);
  Chip_GPIO_SetPinOutLow(LPC_GPIO, 0, 3);
  Chip_IOCON_SetPinConfig(LPC_IOCON, IOCON_PIO0_7, (IOCON_FUNC_0 | IOCON_RMODE_PULLDOWN));
  Chip_GPIO_SetPinDIROutput(LPC_GPIO, 0, 7);
  Chip_GPIO_SetPinOutLow(LPC_GPIO, 0, 7);
  Chip_IOCON_SetPinConfig(LPC_IOCON, IOCON_PIO0_8, (IOCON_FUNC_0 | IOCON_RMODE_PULLDOWN));
  Chip_GPIO_SetPinDIROutput(LPC_GPIO, 0, 8);
  Chip_GPIO_SetPinOutLow(LPC_GPIO, 0, 8);
  Chip_IOCON_SetPinConfig(LPC_IOCON, IOCON_PIO0_9, (IOCON_FUNC_0 | IOCON_RMODE_PULLDOWN));
  Chip_GPIO_SetPinDIROutput(LPC_GPIO, 0, 9);
  Chip_GPIO_SetPinOutLow(LPC_GPIO, 0, 9);
  // Chip_IOCON_SetPinConfig(LPC_IOCON, IOCON_PIO0_10, (IOCON_FUNC_0 | IOCON_RMODE_PULLDOWN));
  // Chip_GPIO_SetPinDIROutput(LPC_GPIO, 0, 10);
  // Chip_GPIO_SetPinOutLow(LPC_GPIO, 0, 10);
  // Chip_IOCON_SetPinConfig(LPC_IOCON, IOCON_PIO0_11, (IOCON_FUNC_0 | IOCON_RMODE_PULLDOWN));
  // Chip_GPIO_SetPinDIROutput(LPC_GPIO, 0, 11);
  // Chip_GPIO_SetPinOutLow(LPC_GPIO, 0, 11);
}

void led_blink(void)
{
  Chip_GPIO_SetPinToggle(LPC_GPIO, 0, 2);
}

void led_display(void)
{
  static uint8_t seg = 0;
  uint8_t var = 0;
  Chip_GPIO_SetPortOutHigh(LPC_GPIO, 0, 0x00000070);
  //TODO set pin output low should have jtag pins.
  Chip_GPIO_SetPortOutLow(LPC_GPIO, 0, 0x00000387);

  var = led_disp_buf[seg];
  if ((var & 0x80) == 0x80)
  {
    Chip_GPIO_SetPinOutHigh(LPC_GPIO, 0, 2);
  }
  if ((var & 0x40) == 0x40)
  {
    Chip_GPIO_SetPinOutHigh(LPC_GPIO, 0, 7);
  }
  if ((var & 0x20) == 0x20)
  {
    Chip_GPIO_SetPinOutHigh(LPC_GPIO, 0, 8);
  }
  if ((var & 0x10) == 0x10)
  {
    Chip_GPIO_SetPinOutHigh(LPC_GPIO, 0, 9);
  }
  if ((var & 0x08) == 0x08)
  {
    Chip_GPIO_SetPinOutHigh(LPC_GPIO, 0, 1);
  }
  if ((var & 0x04) == 0x04)
  {
    Chip_GPIO_SetPinOutHigh(LPC_GPIO, 0, 0);
  }
  if ((var & 0x02) == 0x02)
  {
    Chip_GPIO_SetPinOutHigh(LPC_GPIO, 0, 3);
  }

  Chip_GPIO_SetPinOutLow(LPC_GPIO, 0, led_seg_pin[seg]);
  seg++;
  if (seg >= 5)
  {
    seg = 0;
  }
}

/******************************** END OF FILE *********************************/
