/**
******************************************************************************
* @file    bsp.c 
* @author  jobs
* @version v0.00
* @date    2021-09-19
* @brief   
* @note    
*
******************************************************************************
*/

#include "bsp.h"

void bsp_init(void)
{
  Chip_Clock_System_SetClockFreq(4000000);
  Chip_Clock_System_BusyWait_us(1000);

  Chip_IOCON_Init(LPC_IOCON);
  Chip_GPIO_Init(LPC_GPIO);
  
  Chip_IOCON_SetPinConfig(LPC_IOCON, IOCON_PIO0_10, (IOCON_FUNC_2 | IOCON_RMODE_PULLUP));
  Chip_GPIO_SetPinDIROutput(LPC_GPIO, 0, 10);
  Chip_GPIO_SetPinOutLow(LPC_GPIO, 0, 10);
  Chip_IOCON_SetPinConfig(LPC_IOCON, IOCON_PIO0_11, (IOCON_FUNC_2 | IOCON_RMODE_PULLUP));
  Chip_GPIO_SetPinDIROutput(LPC_GPIO, 0, 11);
  Chip_GPIO_SetPinOutLow(LPC_GPIO, 0, 11);
  
}

/******************************** END OF FILE *********************************/
