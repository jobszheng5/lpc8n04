/**
 ******************************************************************************
 * @file    bsp.h 
 * @author  jobs 
 * @version v0.0.0 
 * @date    2021-09-19 
 * @brief   
 * @note    
 ******************************************************************************
 */
#ifndef BSP_H 
#define BSP_H 

#if defined(__cplusplus)
extern "C" {
#endif /* __cplusplus*/

#include <stdint.h>

#include "chip.h"

extern void bsp_init(void);



#if defined(__cplusplus)
}
#endif /* __cplusplus*/
#endif

/******************************** END OF FILE *********************************/
