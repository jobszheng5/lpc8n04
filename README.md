# LPC8N04
## 简介
感受NXP公司的LPC8N04系列MCU，充分认识NFC的性能。  
使用NXP公司的OM40002评估板为硬件基础  
使用IAR开发软件编译与调试代码  
使用VScode编写、阅读代码  

## 日志
### Sep 19
新建IAR开发环境
将主频设置为4MHz，并使用systick外设，产生1ms的时基。

### Nov 6
使用PIO0_2,PIO0_6两个引脚控制LED灯D9以2Hz的频率闪烁

### Nov 7
使用扫描的方式显示点阵LED屏的效果。
使用Timer16_0的CH0做为定时器，周期4ms，刷新LED矩阵。

### Nov 13
将跳线设置为LED灯矩阵后，通过扫描的方式显示5x7矩阵LED。
通过延迟设置JTAG复用引脚的方式规避jtag无法再次连接的问题。

